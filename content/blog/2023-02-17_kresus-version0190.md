Title: Kresus 0.19
Date: 2023-02-17 08:00
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-19-0

Cette fois-ci on a mis moins d'un an à sortir une nouvelle version, et on a quelques fonctionnalités intéressantes à vous proposer !

## Nouvelles fonctionnalités

### Opérations récurrentes
Il est parfois utile lorsqu'on souhaite avoir une idée de notre balance en fin de mois de renseigner les opérations qui vont arriver, avant même qu'elles n'existent sur le site de la banque. Le faire chaque mois pour celles qui sont régulières, comme un loyer, peut être fastidieux.

Kresus peut maintenant les créer automatiquement pour vous, à la date que vous souhaitez : allez dans « Accès bancaires » et cliquez sur l'icône calendrier correspondant au compte pour lequel vous souhaitez créer une opération récurrente. Un raccourci est également disponible dans les détails des opérations pour créer rapidement une opération récurrente à partir d'une opération existante.

![Écran de création d'une opération récurrente](../images/blog/019-recurring-transaction-screen.png)

## Autres nouveautés
- On peut désormais utiliser une opération existante comme modèle pour créer une nouvelle règle d'auto-catégorisation : dans les détails de l'opération, cliquez sur « Créer règle d'auto-catégorisation »
- Une nouvelle règle d'auto-catégorisation, optionnelle, est disponible : le montant. Le label est toujours obligatoire.
- Il devient possible d'éditer la date des opérations manuelles, utile notamment pour les repousser un peu lorsqu'on s'en sert pour prévisualiser la balance en fin de mois
- Lorsqu'on crée un accès manuel, on peut désormais spécifier la devise
- Dans la vue mobile des budgets la barre de progression peut être cliquée pour accéder aux opérations ciblées
- Graphiques par catégories : lorsqu'on clique sur une catégorie pour l'afficher/cacher, c'est effectif sur tous les graphiques
- Améliorations de l'interface (thème "dark" pour les notifications, graphiques de balance plus légers, …)

## Corrections
- La balance en cours n'était pas recalculée après modification de la date d'une opération.

## Sous le capot
- En base de données la colonne `vendorId` de la table `Account`, redondante avec la table `Access`, est supprimée.
- Mise à jour de dépendances

## Notes de mise à jour
Pour les sysadmins, devops et autres mainteneuses attentives, voici un résumé
des actions à effectuer pour mettre à jour vers cette version de Kresus.

- La version minimale de woob passe à la 3.1.
- La version minimale de node passe à la 16.

## Notre plus grande histoire d'amour, c'est vous
Comme chaque fois, un grand merci aux contributeurices de Kresus !

Merci à celleux qui ont posté sur notre [forum](https://community.kresus.org)
ou avec qui nous avons parlé directement, pour
leurs retours, questions, encouragement, support, etc.

Merci aux mainteneur.se.s du [paquet
YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) et aux gens qui ouvrent
des tickets quand ça ne marche pas sur YunoHost !

Merci aux contributeur.ice.s en code !

Merci aux [donateur.ice.s](https://liberapay.com/Kresus/), qui nous aident non seulement à financer l'infrastructure (site web, nom de domaine), mais aussi qui nous motivent à continuer le projet et à le rendre meilleur qu'il n'est aujourd'hui !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou via [Matrix
(#kresus:delire.party)](https://matrix.to/#/#kresus:delire.party) !
