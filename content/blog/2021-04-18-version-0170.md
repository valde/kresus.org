Title: Kresus 0.17
Date: 2021-04-18 17:00
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-17-0

Un an et dix-huit "confinements" plus tard, il est l'heure de sortir une nouvelle version de Kresus, avec quelques nouvelles fonctionnalités attendues depuis un moment et qui devraient vous simplifier la vie !

## Nouvelles fonctionnalités

### Refonte de l'interface

Beaucoup de soin a été pris pour simplifier et moderniser l'interface. Vous pouvez dire adieu aux imposantes fenêtres flottantes dites *modales* ! Nous les avons remplacées par des micro-fenêtres contextuelles pour les demandes de confirmation simples, et par des écrans séparés pour la création et la modification. Les formulaires ont été également généralement revus, pour être plus uniformes, et plus lisibles (mais également plus rapides à développer !). Au final, l'interface est plus claire, suivant le principe d'un écran par action qui demande de l'attention.

![Exemple de nouveau formulaire]({static}/images/blog/017-form.png)

![Exemple de micro fenêtre de confirmation]({static}/images/blog/017-popover.png)

Il est désormais possible de choisir entre une interface à largeur fixe ou extensible (pour utiliser toute la largeur des grands écrans). Le mode à largeur fixe est le nouveau mode par défaut, étant plus lisible à notre avis ; rendez-vous dans les préférences de personalisation pour essayer et comparer les deux !

### Auto-catégorisation

Une des fonctionnalités les plus anciennes et les plus demandées arrive enfin dans Kresus : la capacité à catégoriser automatiquement les transactions, en fonction du texte de leur libellé ! 🎉

A l'heure actuelle, il est nécessaire de créer les règles de catégorisation vous même, en allant dans la nouvelle section "auto catégorisation". Un écran de création vous permet de créer de nouvelles règles, de la forme *si le label contient le texte Palais Vivienne, alors catégoriser la transaction en tant que Réunion avec 6 ami.e.s dans le respect le plus total des gestes barrières*.

![Auto catégorisation]({static}/images/blog/017-autocategorisation.png)

Les rapports emails de nouvelles transactions vous indiqueront également les catégories associées aux transactions, que ces catégories aient été définies automatiquement ou par vous dans le passé.

Nous aimerions bien pouvoir aller plus loin, dans les prochaines versions de Kresus, notamment en déduisant de votre usage quelles sont les règles et en vous proposant de les ajouter. Ou pourquoi pas, en ayant un petit assistant qui remarque, lors d'une session d'utilisation de Kresus, que vous avez catégorisé plusieurs transactions similaires avec la même catégorie, et vous propose de créer la règle pour vous. Si ça vous intéresse de contribuer ce genre de choses, [venez-nous voir](https://framagit.org/kresusapp/kresus) 👋.

### Support de l'authentification multi-facteurs (2FA)

Certains sites de banque en ligne vous demandent de confirmer votre connexion au site Web, via une application mobile ou un code reçu par SMS. Auparavant, cela bloquait la connexion à Kresus, puisqu'il n'avait pas la possibilité de récupérer ce code. Dorénavant, lorsque vous effectuez une connexion depuis Kresus pour la première fois (ou tous les 90 jours, si votre banque respecte les bonnes pratiques liées), Kresus saura vous demander ce code de confirmation. Si votre banque joue bien le jeu, vous ne devriez être embêté qu'une seule fois tous les 90 jours, car Kresus mémorise les informations de connexion entre deux connexions au site de banque. Si ce n'est pas le cas, n'hésitez-pas à contacter votre conseiller bancaire pour lui indiquer votre mécontentement et essayer de trouver une solution !

![Demande de 2fa]({static}/images/blog/017-2fa.png)

### Vue agrégée par devise

En attendant d'avoir des vues complètement définies par les utilisatrices, qui vous permettraient de voir seulement un sous-ensemble prédéfini de comptes, une nouvelle fonctionnalité arrive dans Kresus : la possibilité de voir toutes les transactions et les graphiques liés à une devise donnée. Pour afficher les données via cette vue, cliquez sur le montant total par devise, indiqué en haut à gauche du menu latéral. Particulièrement utile si vous avez des comptes dans plusieurs pays, avec plusieurs devises, ou si vous voulez avoir des statistiques complètes sur tous vos accès bancaires !

### Sous le capot

Lors de cette version, beaucoup de travail de réécriture a été effectué, mettant un terme à celui commencé dans la version précédente :

- l'intégralité du serveur et du client ont été réécrits en TypeScript
- l'intégralité du code client a été réécrit en réutilisant des composants React modernes (uniquement des composants fonctionnels !).
- le code CSS a été entièrement réorganisé également pour être plus proche des composants React qu'il affecte, et plus facile à maintenir dans la durée.

L'objectif de toutes ces réécritures était de nous permettre d'avancer plus vite dans le développement, en ayant moins à nous soucier des bugs, et en instaurant des composants réutilisables qui faciliteront le développement par la suite. Nous pensons que cela a été efficace, mais c'est un travail de longue haleine que nous ne pourrons juger que dans la durée.

### Mais aussi...

- Les budgets indiquent désormais le montant total des transactions non catégorisées, pour vous indiquer le total des transactions non budgetisées.
- Il était possible de créer par hasard plusieurs budgets pour le même compte, même mois et même année, ce qui pouvait ensuite provoquer des inconsistences d'affichage. Ce désagrément a été réglé dans cette version, et la base de données sera automatiquement nettoyée de ces budgets doublons inutiles.
- La liste des transactions groupe désormais les transactions par mois et année, pour une meilleure lisibilité globale.
- Lors d'une fusion de doublons, la balance après éventuelle fusion est indiquée, pour vous aider à prendre la décision (ou non) de fusionner les deux transactions en question.
- Le mode sombre est désormais également actif lors de la procédure d'accueil des nouvelles utilisatrices. Si l'utilisateur ne l'a pas choisi explicitement, l'utilisation du mode sombre sera déduite des préférences systèmes.
- Beaucoup de petits messages d'explication ont été ajoutés par ci et par là pour ajouter du contexte et expliquer certaines fonctionnalités plus en détail. Nous espérons que vous apprécierez 🙂

## Notes de mise à jour

Pour les sysadmins, devops et autres mainteneuses attentives, voici un résumé
des actions à effectuer pour mettre à jour vers cette version de Kresus.

### Le grand renommage

Weboob, l'outil sous-jacent que Kresus utilise pour se connecter au site de votre banque, a été renommé Woob, pour notre plus grand plaisir ! Nous avons donc renommé "weboob" partout en "woob", notamment dans la configuration, ce qui peut nécessiter quelques actions de votre part :

- [le fichier `config.ini`](https://framagit.org/kresusapp/kresus/-/raw/815ba375c693a5f05d301fa6902f95e3ba468f8a/support/docker/config.example.ini) doit être mis à jour pour mentionner une section "woob" au lieu de "weboob"
- les variables d'environnement utilisent WOOB en lieu et place de WEBOOB auparavant
- si vous utilisiez Docker ou Docker Compose, les variables d'environnement doivent être modifiés, et le point de montage a été renommé de "/weboob" à "/woob" également.

### Montées de version

Kresus nécessite désormais **Python 3** et **Woob 3.0** (dernière version stable) au minimum pour fonctionner.

### Connexion à Postgres via une socket Unix

Il est désormais possible de se connecter à la base de données Postgres en utilisant une socket Unix. Pour cela, vous pouvez indiquer le chemin vers la socket Unix au lieu de l'URL de la base de données. Référez-vous au nouvel [exemple de fichier de configuration](https://framagit.org/kresusapp/kresus/-/raw/815ba375c693a5f05d301fa6902f95e3ba468f8a/support/docker/config.example.ini) pour plus de détails !

## Notre plus grande histoire d'amour, c'est vous

Comme chaque fois, un grand merci aux contributeurices de Kresus !

Merci à celleux qui ont posté sur notre [forum](https://community.kresus.org)
ou avec qui nous avons parlé directement, pour
leurs retours, questions, encouragement, support, etc.

Merci à [Natouille](https://mastodon.tetaneutral.net/@Natouille) et à [Romu70](https://twitter.com/romu70) pour leur influence et leurs propositions en interface utilisatrice et ergonomie. Vous avez été une grande source d'inspiration pour cette version !

Merci aux mainteneurs du [paquet
YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) et aux gens qui ouvrent
des tickets quand ça ne marche pas sur YunoHost !

Merci aux contributeur.ice.s en code : dans l'ordre alphabétique, merci à bnjbvr, framasky, nicodel, nicofrand, oschwand, sinopsisHK, et ZeHiro !

Merci aux [donateur.ice.s](https://liberapay.com/Kresus/), qui nous aident non seulement à financer l'infrastructure (site web, nom de domaine), mais aussi qui nous motivent à continuer le projet et à le rendre meilleur qu'il n'est aujourd'hui !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou via [Matrix
(#kresus:delire.party)](https://matrix.to/#/#kresus:delire.party) !
