Title: Rétrospective de l'année 2018
Date: 2019-01-11 19:04
Lang: fr
Slug: 2018-retrospective

L'année 2018 s'est achevée et malgré la publication d'une seule version de Kresus au cours de cette période, beaucoup de très bonnes choses sont arrivées ! C'est l'occasion de faire une petite rétrospective pour vous dévoiler ce qui se trame pour la prochaine version, vous donner des nouvelles et qui sait, peut-être vous donner envie de contribuer également.

## La communauté

On a fait du chemin depuis les débuts solitaires de Kresus ! Et quand on dit "on a fait", le "on" a bien évolué depuis 1 an : de nouveaux contributeur.ice.s se montrent, et on en est très heureux.se.s ! On a encore du chemin à faire avant que la contribution soit [accessible pour tout le monde](http://www.maiwann.net/blog/designers-&-logiciels-libres-si-on-collaborait/), mais on s'améliore !

* Cette année nous avons pu effectuer des tests utilisateur.ice.s qui nous ont permis de détecter des problèmes d'expérience utilisateur.ice marquants et simples à régler. Kresus est un logiciel qui a vocation à être utilisé (sans blagues !), donc le rendre plus simple à utiliser est une priorité pour nous. Pour vous dire : en quelques minutes d'observation de tests, deux pages de notes de feedback ont été prises et certains des retours ont déjà été adressés et corrigés. Merci en particulier à [Maiwann](http://www.maiwann.net) pour ses précieux conseils ainsi qu'à tou.te.s nos testeur.se.s !
* De nombreux.ses utilisateur.ices nous ont communiqué leurs soucis, que ce soit sur le [forum](https://community.kresus.org/), via les [médias](https://tutut.delire.party/@kresus/) [sociaux](https://twitter.com/kresusapp) ou avec des [tickets](https://framagit.org/kresusapp/kresus) sur notre forge logicielle. Dans tous les cas, nous avons accueilli ces retours avec beaucoup de plaisir et d'humilité.
* Nous avons eu un total de [huit contributeur.ice.s](https://github.com/kresusapp/kresus/graphs/contributors?from=2018-01-01&to=2018-12-31&type=c) en code pour un total de 413 commits sur l'année. Nous avons également eu l'occasion d'organiser une rencontre physique (en mode *hackathon* mais sans le côté malsain) avec l'ensemble des développeurs principaux, en Mars. Cette rencontre a été l'occasion de goûter une bière sombre brassée pour l'occasion, la *Kresus Stout* (image ci-dessous).
* Le projet devient plus communautaire à l'occasion de la création des groupes KresusApp sur [github](https://github.com/kresusapp/) et sur [framagit](https://framagit.org/kresusapp). Kresus n'est plus le projet de seulement une personne, et Benjamin est revenu dessus sur son [blog personnel](https://blog.benj.me/2018/08/23/4-ans-de-kresus-bonjour-kresus-org).

![Photo de la Kresus stout](../images/blog/kresus-stout.jpeg)

## Les fonctionnalités

### Ce qu'on a fait

* Réécrire le code de style nous-même, et se passer de *bootstrap* : cela nous permet d'assurer une meilleure maintenabilité pour la suite, de ne pas dépendre d'un gros *framework* (ce qui allège donc la taille du code initial à charger et réduit le temps de chargement initial). Comme exemple de modifications plus personnelles que l'on a pu faire grâce à ce changement, cela nous a permis de créer un système de thèmes et de mettre en place de nouveaux thèmes (dont un thème léger et un thème sombre).
* Les sélecteurs de catégories et de types essaient désormais de prédire intelligemment ce que vous désirez écrire ; vous pouvez écrire les premières lettres d'une catégorie (ou n'importe quelles lettres présentes) pour que Kresus vous propose le nom complet, ou vous pouvez directement créer de nouvelles catégories depuis la même interface.
* Une grosse partie du code a été réécrite pour préparer la migration vers une base de données relationnelles standard de type SQL, ainsi que pour gérer plusieurs utilisateur.ice.s au sein d'une seule instance de Kresus. On en est encore loin, mais on avance !
* La section des budgets s'améliore : nous avons essayé de rendre les explications plus claires, et il est désormais possible de définir des budgets différents par mois, quand Kresus permettait auparavant uniquement d'utiliser le même budget pour tous les mois.
* Il est désormais possible de renommer un compte avec un libellé personnalisé, pour ne plus avoir des *COMPTE CHEQUE MONSIEUR MADAME UNETELLE* mais quelque chose de plus parlant comme *Compte joint*. Rendez-vous dans les préférences pour utiliser cette fonctionnalité !
* La gestion des sessions avec l'intermédiaire de connexion aux sites web (weboob) est maintenant améliorée, limitant le nombre de connexions simulées à votre banque. Cela prépare le terrain également pour les connexions plus compliquées à deux facteurs (quand on vous demande un code unique après vous être connecté).
* Le nombre de doublons pour le compte en cours d'observation est désormais affiché dans le menu de doublons directement.
* Les performances sont un chantier continu, et nous avons encore essayé de les améliorer dans cette version de Kresus, notamment dans la liste des opérations.
* La nouvelle section "à propos" vous donne des informations sur le projet, comment rentrer en contact avec la communauté et liste également les nombreux autres projets libres sur lesquels s'appuie Kresus.
* La communauté a essayé de rendre l'installation de Kresus encore plus simple, en passant par des améliorations pour les images Docker (pour qu'elles soient plus légères, un docker-compose par défaut ainsi qu'un exemple d'intégration avec Traefik), ainsi que la création d'un [un paquet dédié pour ArchLinux](https://www.archlinux.org/packages/community/x86_64/kresus/), et un début très prometteur de paquet [YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) pour rendre l'installation très simple au plus grand nombre.
* La détection des doublons s'améliore légèrement ! Du travail est encore prévu dessus, mais la détection des comptes en doublon et des opérations en doublon a été améliorée cette année.
* Le support pour les comptes à débit différé (via des comptes de cartes de crédit dont le solde est remis à zéro en début de mois) a été commencé.
* Et bien sûr toutes les nouveautés dont nous avons déjà parlées dans [le dernier article de blog](https://kresus.org/blog/kresus-version-0-13-0.html) : la possibilité d'exclure un compte du solde total d'un accès bancaire, l'accueil des nouveaux utilisateur⋅ice⋅s, la possibilité d'assigner une opération (comme un revenu) au budget du mois précédent ou suivant, la présentation des logs serveurs directement dans le client en y excluant les informations personnelles, etc.

### Ce qu'on veut faire

Kresus est loin d'être fini, et beaucoup de fonctionnalités sont encore à mettre en place ! Parmi les prochains chantiers, quelques arlésiennes mais aussi des choses plus simples à implémenter qui pourraient bien vous aider :

* L'utilisation d'une base de données relationnelles SQL comme support de stockage des données, compatible avec les grands noms du SQL (PostgreSQL, MariaDB, sqlite pour tester).
* Une meilleure détection des doublons, pour que Kresus ne vous affiche plus jamais d'informations fausses et que la plupart des cas soient gérés automatiquement. Aussi la possibilité de marquer des doublons détectés par l'app comme n'en étant pas en réalité (faux positifs).
* La création d'alertes plus utiles, qui vous indiqueront quand Kresus n'arrive plus à se connecter au site de votre banque, ou si les données que Kresus a reçues au cours du temps sont incohérentes et demandent un peu d'attention de votre part.
* La possibilité d'avoir plusieurs utilisateur.ice.s au sein d'une même instance de Kresus, pour pouvoir héberger sa famille et ou ses ami.e.s et ou ses ennemi.e.s.
* L'affectation automatique des catégories aux opérations, avec une interface simple basée sur des libellés.
* La gestion de comptes à la main, c'est-à-dire qui ne correspondent à aucun compte réel du côté de votre banque (par exemple, pour gérer les actifs ou vos dépenses en espèces).
* Des graphiques plus complets, avec des vues spécialisées sur plusieurs comptes de plusieurs banques, plus simples à comprendre.

### Nous avons besoin de vous !

Oui, vous ! Toi, même ! Il y a énormément de manières de contribuer au logiciel libre, sans même savoir coder, il est possible :

* de parler du projet autour de soi, en expliquant pourquoi une alternative à un service de finances personnelles est important (un indice : vos données bancaires en disent très long sur vous) ;
* de donner son avis sur l'utilisation du logiciel via notre [forum](https://community.kresus.org/) ou notre salon de [tchat](https://kiwiirc.com/client/chat.freenode.net/kresus). Vous pouvez nous dire ce qui vous plaît, ce qui vous déplaît, ce qui vous manque ! On ne peut pas garantir que l'on pourra tout résoudre en deux coups de baguette magique, mais on fera de notre mieux pour apprendre de vos retours. Cela nous permet également de prioriser certaines fonctionnalités par rapport à d'autres ! Et si vous ne désirez pas avoir à installer le projet, vous pouvez le tester sur notre instance de [démo](http://demo.kresus.org/) !
* de rajouter de la documentation sur le [site officiel](https://kresus.org) ! Notre documentation est un peu dense, et nous aimerions notamment la séparer en de plus petites parties, plus ciblées sur les démarches à effectuer pour installer et maintenir le projet à jour.
* de créer des paquets pour diverses plateformes (comme ceux déjà créés ou améliorés cette année pour ArchLinux, YunoHost ou encore Docker), afin que Kresus soit simple à installer pour le plus grand nombre ! N'hésitez pas à demander de l'aide et à participer au packaging pour des NAS, des distributions, etc. !
* si vous vous en sentez l'âme, de nous aider à financer le maintien du site web et de la démo, ainsi que notre prochaine rencontre (autour d'un ballon, un demi, une pinte ou un thé), via notre page [Liberapay](https://liberapay.com/Kresus).

C'est tout pour le moment, merci à vous d'avoir lu jusque là et nous espérons que vous profiterez d'une nouvelle année de finances personnelles reposées avec Kresus !
