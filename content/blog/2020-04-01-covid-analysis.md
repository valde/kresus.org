Title: Covid-19 : voici ce que vos données disent sur son impact sur votre vie
Date: 2020-04-01 14:00
Lang: fr
Slug: covid-19-analyse

Avec le contexte sanitaire actuel, la vie et les habitudes de chaque Français ont brutalement changé. Comme nos partenaires #StartupNation de la #FinTech, nous avons décidé d'analyser la situation avec les données auxquelles nous avons accès pour éclairer tout ça (#bigdata <3).

Nos data scientists se sont penché⋅e⋅s sur vos données bancaires pour en sortir une étude donnant un éclairage sur les changements de consommation mais aussi de rythme de vie des utilisateur.ice.s de Kresus ces dernières semaines.


## Une surveillance accrue de vos comptes ?

Dans le contexte inquiétant de la crise économique qui entoure la pandémie mondiale, les nombres ne mentent pas ! Votre utilisation quotidienne de Kresus explose complètement, avec des pics jusqu'à 3 visites quotidiennes :

![Nombre de connexions par jour de la semaine]({static}/images/blog/2020-april-fool-connections.svg)

Graphique du nombre de connexions uniques à mon instance au cours de la semaine passée. Vendredi j'ai redémarré ma box, l'IP a changé.

## L'évolution du nombre de transactions

Ce n'est pas seulement la fréquence des visites qui a changé, mais également le nombre de transactions totales parmi tou.te.s nos utilisateur.ice.s ! Voyez par vous-mêmes le graphique ci-dessous montrant toutes les transactions de tout le monde dans Kresus, les chiffres sont formels !

![Error drawing graph]({static}/images/blog/2020-april-fool-error-drawing-graph.png)

## En résumé

Ah. Bah non en fait. **On n'a pas de statistiques. On ne collecte pas vos données** personnelles. Vos données personnelles sont sur votre serveur, sur votre machine, ou celle de quelqu'un en qui vous avez confiance. On ne sait absolument rien sur vous, d'ailleurs on ne sait même pas combien il y a d'utilisateur.ice.s de Kresus ([faites-nous](https://tutut.delire.party/@kresus/) [signe](https://twitter.com/kresusapp) !). On ne gagne pas d'argent en vous espionnant, ou en collectant vos données bancaires pour les revendre à des partenaires tiers. Et on continue à travailler très fort pour que ça continue !

Si vous aimez ce que l'on fait, n'hésitez-pas à nous [faire un don](https://liberapay.com/Kresus/) ! Plus que le montant, c'est le geste qui nous motive à continuer et à prendre sur notre temps libre pour améliorer Kresus ! Bon confinement !
