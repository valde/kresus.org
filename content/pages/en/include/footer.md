Title: Who are we?
lang: en
status: hidden
is_footer: true

Kresus is a <em>libre</em> software and we are a [community of
contributors](https://framagit.org/kresusapp/kresus/graphs/main) working on it.
We are not affiliated in any ways to Woob or any company and are
independant developers, interested in <em>free, libre and open-source</em>
software, as well as personal finance management apps.

If you like what we do, consider donating through our [![Liberapay logo](/images/pages/liberapay.svg)](http://liberapay.com/kresus).
