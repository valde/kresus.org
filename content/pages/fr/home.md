Title: Kresus - Gestionnaire de finances personnelles
Date: 2019-06-17 18:34:00
Slug: home
Summary: home
lang: fr
save_as: index.html
status: hidden
Template: home

<div class="features" id="firstFeature">
    <div>
        <div>
            <h3>Tous vos comptes, en un coup d'œil&nbsp;!</h3>
            <p>
                Retrouvez l'intégralité de vos comptes dans une même
                interface et visualisez leur solde en un coup d'œil&nbsp;!
            </p>
        </div>

        <p class="screenshot" id="feature-accounts">
            <a href="#feature-accounts-closed" id="feature-accounts-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-accounts">
                <img src="/images/pages/view-all-accounts.png" alt="Capture écran vue globale" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Un budget maîtrisé&nbsp;!</h3>
            <p>
                Configurez simplement des <strong>alertes emails</strong> sur
                montant d'une transaction ou sur solde pour vous
                <strong>prévenir</strong> en cas d'<strong>événements
                    importants</strong> sur vos comptes.
            </p>
        </div>

        <p class="screenshot" id="feature-budget">
            <a href="#feature-budget-closed" id="feature-budget-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-budget">
                <img src="/images/pages/budget.png" alt="Capture écran évolution mois" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Triez, renommez, recherchez vos opérations&nbsp;!</h3>
            <ul>
                <li>Définissez vos propres <strong>libellés</strong>, finis les «&nbsp;<em>Chèque n°168468</em>&nbsp;»&nbsp;!</li>
                <li><strong>Triez</strong> vos opérations par <strong>catégories</strong></li>
                <li>Effectuez des <strong>recherches complexes</strong> sur vos transactions, par période, catégorie,
                    montant ou date !</li>
            </ul>
        </div>

        <p class="screenshot" id="feature-categories">
            <a href="#feature-categories-closed" id="feature-categories-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-categories">
                <img src="/images/pages/categories.png" alt="Capture écran catégories" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Des graphiques clairs&nbsp;!</h3>
            <p>
                Suivez l'évolution de vos mouvements d'argent en un
                clin d'œil avec les graphiques par catégorie, par
                période et par type de mouvement (dépenses ou rentrées d'argent).
            </p>
        </div>

        <p class="screenshot" id="feature-charts">
            <a href="#feature-charts-closed" id="feature-charts-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-charts">
                <img src="/images/pages/charts.png" alt="Capture écran graphiques" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Libre, auto-hébergeable et personnel&nbsp;!</h3>
            <p>
                Qui se sentirait à l'aise à l'idée de partager les identifiants et mots de
                passe de services bancaires en ligne à des parfaits inconnus&nbsp;?
            </p>

            <p>
                Les données bancaires sont <strong>extrêmement personnelles</strong>.
                Elles reflètent des pans entiers de notre vie quotidienne
                et intime&nbsp;: ce que nous achetons, qui nous sommes, ce que nous faisons.
            </p>

            <p>
                Parce que personne d'autre que vous ne devrait connaître
                ces données, Kresus est un logiciel <strong>libre</strong>,
                <strong><em>open-source</em></strong> et
                <strong>auto-hébergeable</strong>.
            </p>

            <p>
                Pour vous garantir les meilleurs résultats, Kresus utilise
                un autre projet libre et open-source très actif,
                <a href="https://woob.tech/">Woob</a>, qui fait le lien
                entre le site bancaire et Kresus.
            </p>

            <p>
                <strong>En utilisant Kresus, vous avez un contrôle total sur le logiciel et vos données&nbsp;!</strong>
            </p>
        </div>

        <p class="screenshot" id="feature-libre">
            <a href="#feature-libre-closed" id="feature-libre-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-libre">
                <img src="/images/pages/framagit.png" alt="Capture écran Framagit" />
            </a>
        </p>
    </div>
</div>
